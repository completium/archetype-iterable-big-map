# The big_map_iterable contract is a contract of which some types can be changed. For the purpose of this example, we will change these types and model a (big_map nat nat) instead of the (big_map address bytes) present in the initial contract

# First, we will apply the desired type to replace types a,b,c and d of the contract

cat big_map_iterable.tz |\
    sed -e 's/(address :a)/(nat :a)/g' |\
    sed -e 's/(bytes :b)/(nat :b)/g' |\
    sed -e 's/(bytes :c)/(nat :c)/g' |\
    sed -e 's/(bytes :d)/(nat :d)/g' \
    > big_map_iterable_nat_nat.tz

# originate the initially empty contract
tezos-client originate contract bgi transferring 0 from bootstrap1 running big_map_iterable_nat_nat.tz --init 'Pair {} (Pair 0 {})' --burn-cap 0.404

# Now we will fill the contract with 10000 entries in batches of 400.
# The transactions are present in the fill.sh

./fill.sh

./delete.sh


# now we can get some informations with the differents views

## get :
tezos-client run view get on contract bgi with input 1 # return 1
tezos-client run view get on contract bgi with input 2000 # return 2000
tezos-client run view get on contract bgi with input 924 # return None (because deleted by delete.sh application)

## get_all :
# the most important view, this view shows how to iterate on the structure
tezos-client run view get_all on contract bgi with input Unit  --gas 100000000

## iter and map :
# The iter and map functions have been written to add examples of iterations.
# They only iterate on values and not on keys (but this would have been possible).

# get the sum of all the values :
tezos-client run view iter on contract bgi with input '(Pair 0 {UNPAIR ; ADD})' --gas 100000000

# get the max value :
tezos-client run view iter on contract bgi with input '(Pair 0 {DUP; UNPAIR ; IFCMPGT{CAR}{CDR}})' --gas 100000000

# get the square of all sores values :
tezos-client run view map on contract bgi with input '{DUP; MUL}' --gas 100000000
