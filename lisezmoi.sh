# Le contrat big_map_iterable est un contrat dont certains types peuvent être changés. Pour les besoins de l'exemple, nous allons justement changer cet types et modéliser une (big_map nat nat) au lieu de la (big_map address bytes) présente dans le contrat initial

# Tout d'abord, nous allons appliquer le type souhaité pour remplacer les types a,b,c et d du contrat

cat big_map_iterable.tz |\
    sed -e 's/(address :a)/(nat :a)/g' |\
    sed -e 's/(bytes :b)/(nat :b)/g' |\
    sed -e 's/(bytes :c)/(nat :c)/g' |\
    sed -e 's/(bytes :d)/(nat :d)/g' \
    > big_map_iterable_nat_nat.tz

# créer le contrat initialement vide
tezos-client originate contract bgi transferring 0 from bootstrap1 running big_map_iterable_nat_nat.tz --init 'Pair {} (Pair 0 {})' --burn-cap 0.404

# Nous allons maintenant remplir le contrat avec 10000 entrées par lots de 400.
# Les transactions sont présentes dans le fichier fill.sh

./fill.sh

./delete.sh


# À présent, nous pouvons obtenir des informations avec les différentes vues.

## get :
tezos-client run view get on contract bgi with input 1 # return 1
tezos-client run view get on contract bgi with input 2000 # return 2000
tezos-client run view get on contract bgi with input 924 # return None (because deleted by delete.sh application)

## get_all :
# La vue la plus importante, cette vue montre comment itérer sur la structure.
tezos-client run view get_all on contract bgi with input Unit  --gas 100000000

## iter and map :
# Les fonctions iter et map ont été écrites pour ajouter des exemples d'itérations.
# Elles n'itèrent que sur les valeurs et non sur les clés (mais cela aurait été possible).

# Obtenir la somme de toutes les valeurs :
tezos-client run view iter on contract bgi with input '(Pair 0 {UNPAIR ; ADD})' --gas 100000000

# Obtenir la valeur maximale :
tezos-client run view iter on contract bgi with input '(Pair 0 {DUP; UNPAIR ; IFCMPGT{CAR}{CDR}})' --gas 100000000

# Obtenir le carré de toutes les valeurs :
tezos-client run view map on contract bgi with input '{DUP; MUL}' --gas 100000000
