const {
  deploy,
  getAccount,
  setQuiet
} = require('@completium/completium-cli');

require('mocha/package.json');

setQuiet('true');

const mockup_mode = true;

// contracts
let iterablebm;

// accounts
const user = getAccount(mockup_mode ? 'bootstrap1' : '');

describe('Contract deployment', async () => {
  it("Iterable_big_map", async () => {
    [iterablebm, _] = await deploy( './iterable_big_map.arl', { as: user.pkh });
  })
})
describe('Setup', async () => {
  it('Add data', async () => {
    await iterablebm.exec({
      arg: [[
        [ "tz1cBsZ7FwtVEMLAL4G9NLNEDXLgATZR8fha", "0x00" ],
        [ "tz1dZydwVDuz6SH5jCUfCQjqV8YCQimL9GCp", "0x01" ],
        [ "tz1h7QQEB1ZCGuLvGZSgQJA87HcBDWUKh4f2", "0x02" ],
        [ "tz1hyc1CRQpjskJUUaGrh85UZXPi6kU4JuGd", "0x03" ]
      ]],
    })
  })

  it('Update', async () => {
    await iterablebm.exec({
      arg: [[ [ "tz1dZydwVDuz6SH5jCUfCQjqV8YCQimL9GCp", "0x05" ] ]]
     })
  })

  it('Remove', async () => {
    await iterablebm.exec({
      arg: [[ ["tz1dZydwVDuz6SH5jCUfCQjqV8YCQimL9GCp", null ] ]]
     })
  })
})